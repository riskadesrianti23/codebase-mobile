import { combineReducers } from 'redux';
import people from '../screens/home/reducer';

const rootReducer = combineReducers({
  people : people
});

export default rootReducer;