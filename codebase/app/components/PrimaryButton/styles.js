import { StyleSheet } from 'react-native';

import { scale, moderateScale, verticalScale } from '../../utils/scaling';

import {
  PRIMARY_COLOR,
  DARK_PRIMARY_COLOR,
  LIGHT_PRIMARY_COLOR,
  COLOR_WHITE,
  FONT_BUTTON
} from '../../styles';

const borderRadius = moderateScale(24);
const font = {
  ...FONT_BUTTON, // fontSize and family
  color: COLOR_WHITE
};

const button = {
  borderRadius,
  height: verticalScale(40),
  width: scale(200)
};

export default StyleSheet.create({
  container: {
    backgroundColor: PRIMARY_COLOR,
    height: verticalScale(40),
    width: scale(200),
    borderRadius
  },
  button: {
    ...button,
    ...font,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  buttonPressed: {
    ...button,
    backgroundColor: DARK_PRIMARY_COLOR
  },
  buttonDisabled: {
    ...button,
    backgroundColor: LIGHT_PRIMARY_COLOR
  }
});
