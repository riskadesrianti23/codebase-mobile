import React from 'react';
import { TouchableHighlight, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: 'normal'
    };
  }

  render() {
    const { disabled, title, onPress, customButtonStyle } = this.props;
    const { status } = this.state;
    let buttonStyle = styles.container;

    if (status === 'pressed') buttonStyle = styles.buttonPressed;
    if (disabled) buttonStyle = styles.buttonDisabled;
    if (disabled) {
      return (
        <TouchableHighlight style={styles.container}>
          <Text style={styles.button}>{title}</Text>
        </TouchableHighlight>
      );
    }
    return (
      <TouchableHighlight
        underlayColor={'transparent'}
        onShowUnderlay={() => this.setState({ status: 'pressed' })}
        onHideUnderlay={() => this.setState({ status: 'normal' })}
        onPress={onPress}>
        <View style={[buttonStyle, customButtonStyle]}>
          <Text style={styles.button}>{title}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

Component.propTypes = {
  disabled: PropTypes.bool,
  title: PropTypes.string,
  onPress: PropTypes.func,
  customButtonStyle: PropTypes.object
};
