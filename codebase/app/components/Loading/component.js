import React from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { PRIMARY_COLOR } from '../../styles';


export default class Component extends React.Component {
  render() {
    const { text, customStyles } = this.props;
    const containerStyles = customStyles || styles.container;
    const textStyles = customStyles || styles.text;

    return (
      <View style={containerStyles}>
        <ActivityIndicator color={PRIMARY_COLOR} size="large" />
        <Text style={textStyles}>{text}</Text>
      </View>
    );
  }
}

Component.propTypes = {
  text: PropTypes.string,
  customStyles: PropTypes.object
};