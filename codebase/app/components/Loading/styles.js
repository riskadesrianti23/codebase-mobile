import { StyleSheet } from 'react-native';

import { widthByScreen, heightByScreen } from '../../utils/dimensions';

import {
  PRIMARY_COLOR,
  COLOR_OPACITY,
  FONT_CAPTION
} from '../../styles';

const font = {
  ...FONT_CAPTION, // fontSize and family
  color: PRIMARY_COLOR
};

const container = {
  width: widthByScreen(100),
  height: heightByScreen(100),
};

const styles = StyleSheet.create({
  container: {
    ...container,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
    backgroundColor:COLOR_OPACITY
  },
  text: {
    ...font
  }
});

export default styles;
