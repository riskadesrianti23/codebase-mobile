import images from './images';
import { services, endpoint } from './services';

export const IMAGES = images;
export const SERVICES = services;
export const ENDPOINT = endpoint;