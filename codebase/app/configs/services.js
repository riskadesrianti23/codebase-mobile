export const services = {
  BASE_URL: 'https://swapi.co/api/',
  TERMINAL: '...',
  API_KEY: '...',
  HEADER_CONTENT_TYPE: 'application/json',
  HEADER_AUTH: '...',
};

// end point
export const endpoint = {
  getPeople : services.BASE_URL + 'people'
};