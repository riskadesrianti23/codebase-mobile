import { moderateScale } from '../utils/scaling';

export const PRIMARY_COLOR = '#00aa8d';
export const DARK_PRIMARY_COLOR = '#008975';
export const LIGHT_PRIMARY_COLOR = '#00bf9a';

export const SECONDARY_COLOR = '#58cd85';
export const DARK_SECONDARY_COLOR = '#21b55a';
export const LIGHT_SECONDARY_COLOR = '#a5e0bc';

export const ACCENT_COLOR = '#58cd85';

export const COLOR_TRANSPARENT = 'rgba(0,0,0,0)';

export const COLOR_OPACITY = 'rgba(0,0,0,.5)';

export const COLOR_BLACK = '#000000';

export const COLOR_WHITE = '#ffffff';

export const COLOR_ERROR = '#d0021b';

const PRIMARY_FONT_REGULAR = 'Roboto-Regular';
const PRIMARY_FONT_BOLD = 'Roboto-Bold';
const PRIMARY_FONT_LIGHT = 'Roboto-Light';


const FONT_SIZE_HEADING = moderateScale(24);
const FONT_SIZE_TITLE = moderateScale(20);
const FONT_SIZE_SUBHEADING = moderateScale(16);
const FONT_SIZE_BODY = moderateScale(14);
const FONT_SIZE_CAPTION = moderateScale(12);
const FONT_SIZE_BUTTON = moderateScale(14); 


export const FONT_HEADING = {
  fontFamily: PRIMARY_FONT_BOLD,
  fontSize: FONT_SIZE_HEADING
};

export const FONT_SUBHEADING = {
  fontFamily: PRIMARY_FONT_LIGHT,
  fontSize: FONT_SIZE_SUBHEADING
};

export const FONT_TITLE = {
  fontFamily: PRIMARY_FONT_LIGHT,
  fontSize: FONT_SIZE_TITLE
};

export const FONT_BUTTON = {
  fontFamily : PRIMARY_FONT_LIGHT,
  fontSize : FONT_SIZE_BUTTON
};

export const FONT_BODY = {
  fontFamily : PRIMARY_FONT_REGULAR,
  fontSize : FONT_SIZE_BODY
};

export const FONT_CAPTION = {
  fontFamily : PRIMARY_FONT_REGULAR,
  fontSize : FONT_SIZE_CAPTION
};