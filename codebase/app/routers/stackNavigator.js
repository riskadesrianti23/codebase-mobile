import { StackNavigator } from 'react-navigation';

import Onboarding from '../screens/onboarding';
import Home from '../screens/home';
import Development from '../screens/development';

// Application Stack for first time
export const AppStack = StackNavigator({
  Onboarding: {
    screen: Onboarding,
    navigationOptions: {
      title: 'Home'
    }
  }
}, { headerMode: 'none' });

// Application Stack for second time
export const AuthStack = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: 'Sign In'
    }
  },
  Development: {
    screen: Development,
    navigationOptions: {
      title: 'Sign In'
    }
  }
}, { headerMode: 'none' });
