import { SwitchNavigator } from 'react-navigation';

import { AppStack, AuthStack } from './stackNavigator';

// Parent Stack to switch between first login, app and auth stacks
export default SwitchNavigator(
  {
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: 'App',
  }
);
