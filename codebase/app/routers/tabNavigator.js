import { TabNavigator, TabBarBottom } from 'react-navigation';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from '../screens/home';
import Development from '../screens/development';

import {
  PRIMARY_COLOR,
  COLOR_BLACK
} from '../styles';


export const Tabs =  TabNavigator(
  {
    Home: {
      screen: Home,
      header: null,
    },
    Development: {
      screen: Development,
      header: null,
    },
  },
  {
    headerMode: 'none',
    lazyLoad: true,
    ////// if using icon in tab bar ///////

    // navigationOptions: ({ navigation }) => ({
    //     tabBarIcon: ({ focused, tintColor }) => {
    //         const { routeName } = navigation.state;
    //         let iconName;
    //         if (routeName === "Home") {
    //             iconName = focused ? 'home' : 'home-outline'
    //         } else if (routeName === "Development") {
    //             iconName = focused ? 'heart' : 'heart-outline'
    //         }
    //         return (<Icon name={iconName} size={25} color={tintColor} />)
    //     }
    // }),
    tabBarOptions: {
      activeTintColor: PRIMARY_COLOR,
      inactiveTintColor: COLOR_BLACK
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false
  }
);
