import { StyleSheet } from 'react-native';

import { widthByScreen, heightByScreen } from '../../utils/dimensions';
import Metrics from '../../constants/metrics';

import {
  COLOR_WHITE,
  PRIMARY_COLOR,
  FONT_TITLE
} from '../../styles';


const styles = StyleSheet.create({
  statusBar: Metrics.statusBar,
  container: {
    flex: 1,
    backgroundColor: COLOR_WHITE
  },
  title: {
    ...FONT_TITLE,
    color: PRIMARY_COLOR,
    textAlign: 'center',
    marginTop: 20
  },
  images: {
    marginTop: 60,
    marginBottom: 30,
    alignSelf: 'center',
    width: widthByScreen(80),
    height: heightByScreen(30)
  },
  text_telkomdev: {
    ...FONT_TITLE,
    position: 'absolute',
    alignSelf: 'center',
    color: PRIMARY_COLOR,
    textAlign: 'center',
    bottom: 10
  },
  buttonContainer: {
    marginTop: 30,
    alignItems: 'center'
  }
});

export default styles;