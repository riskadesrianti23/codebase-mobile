import React from 'react';
import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';

import I18n from '../../i18n';
import PrimaryButton from '../../components/PrimaryButton';
import styles from './styles';
import { IMAGES } from '../../configs';


export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };

  }

  _buttonPressed = () => {
    const { navigation } = this.props;
    this.props.actions.fetchPeopleFromAPI();
    navigation.navigate('Development');
  }

  _renderButton() {

    return (
      <View style={styles.buttonContainer}>
        <PrimaryButton title={I18n.t('primaryButton')} customButtonStyle={{ alignSelf: 'center' }} onPress={this._buttonPressed} />
      </View>
    );
  }

  _renderImage() {
    return (
      <Image resizeMode="contain" style={styles.images} source={IMAGES.dev} />
    );
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.statusBar} />
        {this._renderImage()}
        <Text style={styles.title}>{I18n.t('quote')}</Text>
        {this._renderButton()}
      </View>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object,
  actions: PropTypes.object
};
