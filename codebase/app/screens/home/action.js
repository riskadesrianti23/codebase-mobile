import { TYPES } from '../../constants';
import { SERVICES, ENDPOINT } from '../../configs';

export function fetchPeopleFromAPI() {
  return (dispatch) => {
    dispatch(getPeople());
    fetch(ENDPOINT.getPeople, {
      method: 'GET',
      header: {
        'Content-Type': SERVICES.HEADER_CONTENT_TYPE
      }
    })
      .then(data => data.json())
      .then(json => {
        dispatch(getPeopleSuccess(json.results));
      })
      .catch(err => dispatch(getPeopleFailure(err)));
  };
}

function getPeople() {
  return {
    type: TYPES.FETCHING_PEOPLE
  };
}

function getPeopleSuccess(data) {
  return {
    type: TYPES.FETCHING_PEOPLE_SUCCESS,
    data,
  };
}

function getPeopleFailure(err) {
  return {
    type: TYPES.FETCHING_PEOPLE_FAILURE,
    err
  };
}

export default fetchPeopleFromAPI;