import { TYPES  } from '../../constants';

const initialState = {
  people: [],
  isLoading: false,
  error: false
};


export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCHING_PEOPLE:
      return {
        ...state,
        isLoading: true,
        people: []
      };
    case TYPES.FETCHING_PEOPLE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        people: action.data
      };
    case TYPES.FETCHING_PEOPLE_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    default:
      return state;
  }
}