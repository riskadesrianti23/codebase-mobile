import { StyleSheet } from 'react-native';

import { widthByScreen, heightByScreen } from '../../utils/dimensions';
import Metrics from '../../constants/metrics';
import { verticalScale } from '../../utils/scaling';

import {
  PRIMARY_COLOR,
  COLOR_WHITE,
  FONT_TITLE,
  FONT_BODY
} from '../../styles';


const styles =  StyleSheet.create({
  statusBar: Metrics.statusBar,
  container: {
    flex: 1,
    backgroundColor: COLOR_WHITE
  },
  title: {
    ...FONT_TITLE,
    color: PRIMARY_COLOR,
    textAlign: 'center',
    marginTop: verticalScale(20)
  },
  images: {
    marginTop: verticalScale(60),
    marginBottom: verticalScale(20),
    alignSelf: 'center',
    width : widthByScreen(80),
    height : heightByScreen(30)
  },
  text: {
    ...FONT_BODY,
    alignSelf: 'center',
    color: PRIMARY_COLOR,
    textAlign: 'center',
    marginTop: verticalScale(8)
  },
  buttonContainer: {
    marginTop: verticalScale(30),
    alignItems: 'center'
  },
  containerList : {
    height : verticalScale(500)
  }
});

export default styles;