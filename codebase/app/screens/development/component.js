import React from 'react';
import { View, Text, FlatList } from 'react-native';
import PropTypes from 'prop-types';

import I18n from '../../i18n';
import PrimaryButton from '../../components/PrimaryButton';
import styles from './styles';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: ['sam', 'reza', 'ilham', 'anton', 'rama', 'ivan', 'anis', 'winda', 'egi'],
      refreshing: false
    };
  }

  componentDidMount() {
    this._detectOrientation();
  }

  _detectOrientation() {
    if (this.state.Width_Layout > this.state.Height_Layout) {
      this.setState({ OrientationStatus: 'Landscape' });
    }
    else {
      this.setState({ OrientationStatus: 'Portrait' });
    }
  }

  _renderButton() {
    const { navigation } = this.props;
    return (
      <View style={styles.buttonContainer}>
        <PrimaryButton title={I18n.t('back')} customButtonStyle={{ alignSelf: 'center' }} onPress={() => navigation.goBack()} />
      </View>
    );
  }

  _renderHeader = () => {
    return <Text style={styles.title}>{I18n.t('developer')}</Text>;
  };

  _renderItem = (item) => {
    return (
      <Text style={styles.text}>{item}</Text>
    );
  };

  _renderList = () => {
    return (
      <View style={styles.containerList}>
        <FlatList
          style={{}}
          data={this.state.data}
          numColumns={1}
          renderItem={({ item }) => (
            this._renderItem(item)
          )}
          keyExtractor={item => item}
          ListHeaderComponent={this._renderHeader}
        />
      </View>
    );
  };

  render() {
    const { OrientationStatus } = this.state;
    return (
      <View style={styles.container} onLayout={(event) => this.setState({
        Width_Layout: event.nativeEvent.layout.width,
        Height_Layout: event.nativeEvent.layout.height
      }, () => this._detectOrientation())}>
        <View style={styles.statusBar} />
        {OrientationStatus == 'Landscape' ?
          <Text style={styles.title}>check developer in Potrait mode</Text> :
          this._renderList()}
        {this._renderButton()}
      </View>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object
};
