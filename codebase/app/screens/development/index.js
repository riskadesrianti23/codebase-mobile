import { connect } from 'react-redux';

import Component from './component';


function mapStateToProps(state) {
  const { isLoading, people, error } = state.people;
  return {
    people,
    isLoading,
    error
  };
}

export default connect(
  mapStateToProps
)(Component);