#!/usr/bin/python

import sys
import re 

filename = sys.argv[1]
projectname = sys.argv[2]

text = open(filename).read()  # read the entire file content

matches = re.findall(r'cobaCodebase',text)  # match text between two quotes
for m in matches:
  text = text.replace(m, projectname)  # override text to include tags

# write modified text with tags to file
with open(filename, 'w') as f:
  f.write(text)
