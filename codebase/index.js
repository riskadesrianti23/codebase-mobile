import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './app/store/configureStore';
import App from './App';


const store = configureStore();

const Apps = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent('codebase', () => Apps);
