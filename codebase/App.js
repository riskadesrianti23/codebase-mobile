import React, { Component } from 'react';

import Router from './app/routers';

export default class App extends Component {
  render() {
    return (
      <Router />
    );
  }
}