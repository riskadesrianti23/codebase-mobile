IF EXIST "C:\Program Files\Python 3.6.5" (GOTO :eof)
ELSE
(wget https://www.python.org/ftp/python/3.6.5/python-3.6.5-amd64.exe
"python-3.6.5-amd64.exe" /passive TargetDir="C:\Program Files\Python 3.6.5\" PrependPath=1 include_pip=1
@echo "ftype"
ftype Python.File="C:\WINDOWS\py.exe" "%1" %*
assoc .py)

:eof
